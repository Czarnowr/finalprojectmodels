package com.finalproject.models.model;

public enum SupportTicketType {
    ACCOUNT_ERROR,
    TRANSACTION_ERROR,
    BUG_REPORT,
    FEATURE_REQUEST,
    OTHER
}
