package com.finalproject.models.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String UUID;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate transactionDate;

    @Enumerated(value = EnumType.STRING)
    private TransactionType type;

    private Double amountExchanged;

    private Double amountReceived;

    private Double exchangeRate;

    private Double benchmarkExchangeRate;

    private Double DecimalDifferenceFromBenchmarkExchangeRate;

    private Double PercentageDifferenceFromBenchmarkExchangeRate;

    @ManyToOne
    public ExchangeLocation exchangeLocation;

    @ManyToOne
    public Currency currency;
}
