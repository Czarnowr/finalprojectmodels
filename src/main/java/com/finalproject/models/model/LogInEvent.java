package com.finalproject.models.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogInEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String UUID;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @CreationTimestamp
    private LocalDateTime timestamp;

    private String IP;

    @Enumerated(value = EnumType.STRING)
    public LogInEventType eventType;

    @ManyToOne
    public AppUser appUser;
}
