package com.finalproject.models.model;

public enum LogInEventType {
    LOG_IN,
    LOG_OUT
}
