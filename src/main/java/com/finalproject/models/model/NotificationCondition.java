package com.finalproject.models.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationCondition {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private LocalDate activeUntil;

    private Boolean active;

    private Double assetWorthDifference;

    private Double currencyPriceDifference;

    @OneToOne
    public CurrencyDetails currencyDetails;
}
