package com.finalproject.models.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String email;
    private String password;

    private String firstName;
    private String surname;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;

    @ManyToMany
    public Set<AppUserRole> roles;

    @OneToMany(mappedBy = "owner")
    public Set<Currency> currencyWallet;

    @OneToMany
    public Set<NotificationCondition> notificationConditions;

    @OneToMany(mappedBy = "appUser")
    public Set<SupportTicket> supportTickets;

    @OneToMany(mappedBy = "appUser")
    public Set<Error> errors;

    @OneToMany(mappedBy = "appUser")
    public Set<LogInEvent> logInEvents;
}
