package com.finalproject.models.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private Double amountHeld;

    @ManyToOne
    public AppUser owner;

    @ManyToOne
    public CurrencyDetails currencyDetails;

    @OneToMany(mappedBy = "currency")
    public Set<Transaction> transactions;
}
