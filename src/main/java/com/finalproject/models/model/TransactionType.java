package com.finalproject.models.model;

public enum TransactionType {
    BUY,
    SELL
}
